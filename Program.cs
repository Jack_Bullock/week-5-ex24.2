﻿using System;

namespace week_5_ex28
{
    class Program
    {
       static double PrintGreeting (double a, double b, double c)
        {
        
            return a * b * c;
           
        }
        static void Main(string[] args)
        {
            var a = 0.0;
            var b = 0.0;
            var c = 0.0;
            Console.WriteLine("Enter a number");
            a = double.Parse(Console.ReadLine()); 
            Console.WriteLine("Enter another number");
            b = double.Parse(Console.ReadLine()); 
            Console.WriteLine("Enter onemore number");
            c= double.Parse(Console.ReadLine()); 

            Console.WriteLine("EX28 task d");
            Console.WriteLine(PrintGreeting(a,b,c));
           
        
        }
    }
}
